#My enviroment settings for Minecraft 1.7.10.  

##mod list  
* AgriCraft
* AOBD
* AppleCore
* Applied Energistics 2 rv2
	* ExtraCells
* AutoUnify
* Baubles
* BDlib
	* Advanced Generators
	* Gendustry
	* Pressure Pipes
* BiblioCraft
* BigReactors
* bspkrs Core
	* StatusEffectHUD
* BrandonsCore
	* Draconic Evolution
* Buildcraft
	* Buildcraft Compat
* Carpenter's Blocks
* Catwalks 2
* CodeChickenCore
	* ChickenChunks
	* NotEnoughItems
		* NEI Addons
		* NEI Integration
		* Waila
			* WailaAddonBC
			* WAILA Plugins
			* Wawla
* CoFHCore
	* MineFactoryReloaded
	* Thermal Foundation
		* Thermal Dynamics
		* Thermal Expansion
		* RedstoneArsenal
* CTMLib
	* Chisel
* D3Core
	* InsiderTrading
* denseores
* EnderCore
	* EnderIO
		* EnderIO Addons
* EnderZoo
* Et Futurum
* Extra Utilities
* Factorization
* Fastcraft
* FastLeafDecay
* Fluxed Core
* Forestry
* GardenStuff
* Grimoire of Gaia3
* Headcrumbs
* HoloInventory
* ImmersiveEngineering
	* immersiveintegration
* Lite Loader
	* VoxelMap
* Mantle
	* Tinker's Construct
		* IguanaTinkerTweaks
		* ExtraTiC
		* TiCTooltips
		* Tinker's Mechworks
		* Thermal Casting
* mcjtylib
	* RF Tools
* Mekanism
	* MekanismGenerators
	* MekanismTools
* MineTweaker3
	* ModTweaker2
	* ContentTweaker
	* MineTweakerRecipeMaker
* MmmMmmMmmMmm
* More Villager Biomes
* OpenEye
* OreDictionaryConverter
* Pam's HarvestCraft
* QuarryPlus
* Railcraft
* SaferVillage
* StorageDrawers
* UnicodeFontFixer
* VeinMiner
	* VeinMiner Mod Integration
* Water Hooks
* Ztones
